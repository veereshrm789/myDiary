import React from "react";
import { motion } from "framer-motion";
import { Button, Input } from "../../components/atom";
import {IconFacebook, IconGoogle} from "../../assets/icons"
const Register = () => {
  return (
    <motion.div
      initial={{ x: -100 }}
      animate={{ x: 1 }}
      exit={{ x: 100 }}
      transition={{ duration: 0.2 }}>
     
        <div className="flex flex-col items-center min-h-screen pt-6 sm:justify-center sm:pt-0 bg-background">
          <div>
            <a href="/">
              <h3 className="text-4xl font-bold text-primary hover:scale-110  pl-2 py-2">
                {" "}
                Register{" "}
              </h3>
            </a>
          </div>
          <div className="w-full px-6 py-4 mt-6 overflow-hidden bg-white shadow-md sm:max-w-lg sm:rounded-lg">
            <form className="flex flex-col gap-3">
              <Input placeholder="Name" />
              <Input placeholder="Email" type="email" />
              <Input placeholder="Password" type="password" />
              <Input placeholder="Confirm Password" typt="password" />
              <a href="/#" className="text-sm text-gray-600 hover:underline">
                {" "}
                Forget&nbsp;Password?
              </a>
              <div className="flex items-center">
                <Button> Register</Button>
              </div>
            </form>
            <div className="mt-4 text-grey-600">
              {" "}
              Already have an account?{" "}
              <span>
                <a className="text-gray-600 hover:underline" href="/#">
                  Log&nbsp;in{" "}
                </a>
              </span>
            </div>
            <div className="flex items-center w-full my-4">
              <hr className="w-full" />
              <p className="px-3 ">OR</p>
              <hr className="w-full" />
            </div>
            <div className="my-6 space-y-2">
              
              <Button cat="secondary" style={'rounded'} width="full">
                <div className="flex gap-2 justify-center items-center"><IconGoogle fill="primary"/>
                Login with Google</div>
              </Button>
              <Button cat="secondary" style='rounded' width="full">
                <div className="flex gap-2 justify-center items-center"><IconFacebook fill="primary"/>
                Login with Google</div>
              </Button>
            </div>
          </div>
        </div>
    </motion.div>
  );
};

export default Register;
