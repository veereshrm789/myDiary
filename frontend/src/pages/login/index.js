import React, { useState } from "react";
import { IconFacebook, IconGoogle } from "../../assets/icons";
import { Button, Checkbox, Input } from "../../components/atom";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

const Login = () => {
  let navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const submitLogin = async (e) => {
    e.preventDefault();
    try {
      const config = {
        headers: {
          "Content-type": "application/json",
        },
      };
      setLoading(true);
      const { data } = await axios.post(
        "/app/users/login",
        { email, password },
        config
      );
      console.log(data);
      localStorage.setItem("userInfo", JSON.stringify(data));
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(error.response.data.message);
    }
  };
  if (error) {
    return <div>{error}</div>;
  }
  if (loading) {
    return <div>Loading</div>;
  }

  return (
    <div className="h-screen">
      <div className="h-full px-6 text-gray-800 bg-background">
        <div className="flex flex-wrap items-center justify-center h-full xl:justify-center lg:justify-between g-6">
          <div className="mb-12 grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 md:mb-0">
            <img
              src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
              className="w-full"
              alt="Sample image"
            />
          </div>
          <div className="mb-12 xl:ml-20 xl:w-5/12 lg:w-5/12 md:w-8/12 md:mb-0">
            <form onSubmit={submitLogin}>
              <div className="flex flex-row items-center justify-center lg:justify-start">
                <p className="mb-0 mr-4 text-lg">Sign in with</p>
                <Button
                  style="rounded"
                  disable
                  data-mdb-ripple="true"
                  data-mdb-ripple-color="light"
                >
                  {<IconFacebook />}
                </Button>
                <Button
                  type="button"
                  style="rounded"
                  data-mdb-ripple="true"
                  data-mdb-ripple-color="light"
                >
                  {" "}
                  {<IconGoogle />}{" "}
                </Button>
              </div>

              <div className="flex items-center my-4 before:flex-1 before:border-t before:border-gray-300 before:mt-0.5 after:flex-1 after:border-t after:border-gray-300 after:mt-0.5">
                <p className="mx-4 mb-0 font-semibold text-center">Or</p>
              </div>
              <div className="flex flex-col gap-6 md-6">
                {" "}
                <Input
                  placeholder="Email Address"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <Input
                  type="password"
                  value={password}
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>

              <div className="flex items-center justify-between mb-6">
                <Checkbox title="Remember Me" />
                <Link to="/" className="text-gray-800">
                  {" "}
                  Forgot password?{" "}
                </Link>
              </div>

              <div className="text-center lg:text-left">
                <Button type="submit" cat="primary">
                  Login{" "}
                </Button>
                <p className="pt-1 mt-2 mb-0 text-sm font-semibold">
                  {" "}
                  Don't have an account?
                  <Link
                    to="/register"
                    className="m-1.5 text-gray-600 hover:underline transition duration-200 ease-in-out "
                  >
                    Register
                  </Link>
                </p>{" "}
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
