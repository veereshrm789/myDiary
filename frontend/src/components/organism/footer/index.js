import React from "react";

export const Footer = () => {
  return (
    <div className="bottom-0 flex justify-center bg-black w-full text-white bg-neutral-700 ">
      Copyright &copy; My Diary
    </div>
  );
};
