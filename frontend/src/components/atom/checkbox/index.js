export function Checkbox({ title, ...props }) {
  return (
    <div className="form-group form-check" {...props}>
      <input
        type="checkbox"
        className="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200  checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
      />
      <label className="inline-block text-gray-800 form-check-label">
        {title}
      </label>
    </div>
  );
}

Checkbox.defaultProps = {
  title: "Checkbox",
};
