export const Input = ({ title, type, placeholder, ...props }) => {
  return (
    <>
      {title && (
        <label htmlFor="label" className="block text-sm font-medium text-gray-700 undefined">
          {title}
        </label>
      )}
      <input
        type={type}
        name="label"
        className="block w-full px-4 py-2 m-0 text-xl font-normal text-gray-700 transition ease-in-out bg-white border border-gray-300 border-solid rounded form-control bg-clip-padding focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
        placeholder={placeholder}
        {...props}
      />
    </>
  );
};

Input.defaultProps = {
  title: "",
  type: "text",
  placeholder: "",
};
