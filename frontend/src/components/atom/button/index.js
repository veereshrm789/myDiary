export function Button({ type, cat, style, disable,width, children, ...props }) {
  return (
    <>
      <button
        type={type}
        className={
          cat === "primary"
            ? (style === "normal" &&
                `inline-block py-3 text-sm font-medium leading-snug text-white uppercase transition duration-150 ease-in-out ${width && "w-full"}  rounded shadow-md px-7 ${
                  !disable
                    ? "bg-blue-600 hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 active:bg-blue-800 active:shadow-lg"
                    : "border-gray-400 cursor-not-allowed"
                }   focus:shadow-lg focus:outline-none focus:ring-0  `) ||
              (style === "rounded" && 
                `inline-block p-3 mx-1 text-xs font-medium leading-tight ${width && "w-full"}  text-white uppercase transition duration-150 ease-in-out  rounded-full shadow-md ${
                  !disable
                    ? "bg-blue-600 hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg active:bg-blue-800 active:shadow-lg"
                    : "bg-gray-400 cursor-not-allowed"
                }   focus:outline-none focus:ring-0 `)
            : //Secondary Button
              (style === "normal" &&
                `inline-block py-3 text-md ${width && "w-full"} font-semibold leading-snug uppercase transition duration-150 ease-in-out  rounded shadow-md border-2 px-7 ${
                  !disable
                    ? " border-blue-600 text-primary  hover:border-blue-700 hover:shadow-lg focus:border-blue-700 active:-blue-800 active:shadow-lg"
                    : "border-gray-400 text-gray-400  cursor-not-allowed"
                }   focus:shadow-lg focus:outline-none focus:ring-0  `) ||
              (style === "rounded" &&
                `inline-block p-3 mx-1 text-md font-semibold leading-tight ${width && "w-full"}  uppercase transition duration-150 ease-in-out  rounded-full shadow-md border-2${
                  !disable
                    ? " border-blue-600 text-primary hover:shadow-lg focus:border-blue-700 active:border-blue-800 active:shadow-lg"
                    : " border-gray-400 text-gray-400 cursor-not-allowed"
                }   focus:outline-none focus:ring-0 `)
        }
        {...props}>
        {children}
      </button>
    </>
  );
}

Button.defaultProps = {
  type: "button",
  cat: "primary",
  style: "normal",
  width:"",
  disable: false,
};
