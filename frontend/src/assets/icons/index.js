import { IconPlus } from "./iconPlus";
import { IconTrash } from "./iconTrash";
import { IconEdit } from "./iconEdit";
import { IconSearch } from "./iconSearch";
import { IconDelete } from "./iconDelete";
import { IconFacebook } from "./iconFacebook";
import { IconGoogle } from "./iconGoogle";
export {
  IconPlus,
  IconTrash,
  IconEdit,
  IconSearch,
  IconDelete,
  IconFacebook,
  IconGoogle,
};
